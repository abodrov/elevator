import argparse
from threading import Thread, Lock
from sortedcontainers import SortedSet
import time

floors_queue = SortedSet()
lock = Lock()


class ElevatorState:
    moving = 1
    door_opened = 2
    stopped = 3
    door_closed = 4


class Elevator:

    direction = None  # up, down
    state = None
    position = None

    height = None
    # не очень удачное название
    time = None
    speed = None
    floors_count = None

    time_between_floor = None
    tick = None

    def __init__(self, floors_count, height, time, speed):
        self.floors_count = floors_count  # args.floors_count
        self.height = height
        self.time = time
        self.speed = speed

        self.time_between_floor = self.height / self.speed

        # берём пренебрежимо малый тик
        self.tick = min(self.time, self.time_between_floor)/10

        self.position = 1
        self.state = ElevatorState.stopped


    def move(self, step):
        time.sleep(self.time_between_floor)
        Event.print_event("lift moved", self.position)
        self.position += step
        # self._dump()

    def _dump(self):
        print('dump', self.direction, self.state)

    def move_up(self):
        self.move(1)

    def move_down(self):
        self.move(-1)

    def main_loop(self):

        global floors_queue

        while True:
            if self.state == ElevatorState.stopped and len(floors_queue) == 0:
                self.direction = None

            if self.state == ElevatorState.stopped and len(floors_queue) != 0:
                self.state = ElevatorState.moving
                if self.position < floors_queue[0]:
                    self.direction = 'UP'
                if self.position > floors_queue[0]:
                    self.direction = 'DOWN'

            # если двери открыты, то ждем time и закрываем их
            if self.state == ElevatorState.door_opened:
                Event.print_event("door_opened", self.position)
                time.sleep(self.time)
                self.state = ElevatorState.door_closed
                Event.print_event("door_closed", self.position)

            # если мы закрыли двери и больше ничего нет -- стоим на месте
            if self.state == ElevatorState.door_closed and len(floors_queue) == 0:
                self.state = ElevatorState.stopped

            # если мы закрыли двери и надо двигаться дальше -- начинаем двигаться
            if self.state == ElevatorState.door_closed and len(floors_queue) != 0:
                self.state = ElevatorState.moving

            # если очередь не пуста -- двигаемся куда-нибудь
            if self.state == ElevatorState.moving and len(floors_queue) != 0:
                # print('DEBUG: ', floors_queue)  # TODO: logging module

                # если едем вверх
                if self.direction == 'UP':
                    # print('UP ', floors_queue[-1])
                    # ... и нам есть куда двигаться в том же направлении
                    if floors_queue[-1] > self.position:
                        self.move_up()
                    # иначе меняем направление
                    else:
                        self.direction = 'DOWN'

                elif self.direction == 'DOWN':
                    # print('DOWN')
                    if floors_queue[0] < self.position:
                        self.move_down()
                    else:
                        self.direction = 'UP'

            # если едем  мимо этажа с остановкой, то открываем двери
            if self.state == ElevatorState.moving and self.position in floors_queue:
                self.state = ElevatorState.door_opened
                with lock:
                    floors_queue.remove(self.position)
            # если очередь пуста, то спим. строго говоря, тут стоило бы использовать
            # https://docs.python.org/3/library/threading.html#condition-objects
            # но до этого у меня руки не дошли

            if len(floors_queue) == 0:
                # тик
                time.sleep(self.tick)

class Event:
    @staticmethod
    def print_event(action, floor):
        print("action %s occured on %d floor\n" % (action, floor))


class Util:

    @staticmethod
    def main_loop(floor_count):
        global floors_queue

        # тут копипаста, но один раз можно
        while True:
            floor = input('call the elevator?\n')
            if floor is not '':
                print('call the elevator', floor)
                try:
                    if int(floor) < 1 or int(floor) > floor_count:
                        raise ValueError('you do it wrong')
                    with lock:
                        floors_queue.add(int(floor))
                except Exception as e:
                    print('something went wrong', e.__repr__())

            floor = input('press on the button?\n')
            if floor is not '':
                print('press on the button', floor)
                try:
                    if int(floor) < 1 or int(floor) > floor_count:
                        raise ValueError('you do it wrong')
                    with lock:
                        floors_queue.add(int(floor))
                except Exception as e:
                    print('something went wrong', e.__repr__())


def elevator_thread(args):
    elevator = Elevator(
        floors_count=args.floor_count,
        height=args.height,
        time=args.time,
        speed=args.speed
    )

    elevator.main_loop()


def main_thread(floor_count):
    Util.main_loop(floor_count)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Elevator simulator')
    parser.add_argument('--floor_count', type=int, help='floor count', required=True)
    parser.add_argument('--height', type=float, help='height of the floor', required=True)
    parser.add_argument('--speed', type=float, help='speed of the elevator', required=True)
    parser.add_argument('--time', type=float, help='time between open and close', required=True)
    args = parser.parse_args()

    print(args)

    thread1 = Thread(target=elevator_thread, args=(args,))
    thread2 = Thread(target=main_thread, args=(args.floor_count, ))

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()


'''


Программа запускается из командной строки, в качестве параметров задается:

- кол-во этажей в подъезде — N (от 5 до 20);
- высота одного этажа;
- скорость лифта при движении в метрах в секунду (ускорением пренебрегаем, считаем, что когда лифт едет — он сразу едет с определенной скоростью);
- время между открытием и закрытием дверей.

После запуска программа должна постоянно ожидать ввода от пользователя и выводить действия лифта в реальном времени. События, которые нужно выводить:

- лифт проезжает некоторый этаж;
- лифт открыл двери;
- лифт закрыл двери.

Возможный ввод пользователя:

- вызов лифта на этаж из подъезда;
- нажать на кнопку этажа внутри лифта.
'''